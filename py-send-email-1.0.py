#!/usr/bin/env python

import sys
import argparse
import time
import platform
import pprint
import os
# Configuration-related module
import ConfigParser
# Emmail-related modules
import smtplib
import mimetypes
from email import encoders
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def argumentsparsing():
    # parsing users arguments
    # program version
    version = " version 1.0 by Ardya Suryadinata (see README File for more detailed documentation)"
    # program description
    parser = argparse.ArgumentParser(description="This script will send an email" + "\n" + version)
    parser.add_argument("--version", help="print version", action="store_true")
    parser.add_argument("--sender", help="sender email address (REQUIRED)")
    parser.add_argument("--passwd", help="sender email address password (REQUIRED)")
    parser.add_argument("--to", help="recipient(s) email address (REQUIRED)")
    parser.add_argument("--server", help="SMTP server to be used ex: localhost or smtp.gmail.com (REQUIRED)")
    parser.add_argument("--port", help="SMTP server port. (Optional. Default = 25)")
    parser.add_argument("--ssl", help="set to use SSL. (Optional. Default = False))", action="store_true")
    parser.add_argument("--subject", help="email subect (Optional)")
    parser.add_argument("--message", help="email message (REQUIRED)")
    parser.add_argument("--attachment", help="attachment(s) (Optional)")
    parser.add_argument("--attachdir", help="attach all item(s) in --attachment directory or in current working directory (Optional)", action="store_true")
    parser.add_argument("--configfile", help="use config file to set parameters")
    # parsing arguments
    args = parser.parse_args()
    # print version
    if args.version:
        print sys.argv[0].split("\\")[-1] + " " + version
        sys.exit()
    # check if user wants to use config file
    if args.configfile:
        config_values = parse_config_file(args.configfile)
        argumendariuser = {
            'sender': config_values['settings']['sender'],
            'passwd': config_values['settings']['passwd'],
            'to': config_values['settings']['to'],
            'server': config_values['settings']['server'],
            'port': config_values['settings']['port'],
            'ssl': config_values['settings']['ssl'],
            'subject': config_values['settings']['subject'],
            'message': config_values['settings']['message'],
            'attachment': config_values['settings']['attachment'],
            'attachdir': config_values['settings']['attachdir'], }
        
        # check validity of config value
        if argumendariuser['port']:
            try:
                argumendariuser['port'] = int(argumendariuser['port'])
            except:
                argumendariuser['port'] = None
        if not argumendariuser['ssl']:
            argumendariuser['ssl'] = False
        else:
            argumendariuser['ssl'] = True
        if not argumendariuser['attachdir']:
            argumendariuser['attachdir'] = False
        else:
            argumendariuser['attachdir'] = True
        if not argumendariuser['attachment']:
            argumendariuser['attachment'] = None
        
        return argumendariuser
    # check for required arguments
    elif not (args.sender and args.passwd and args.to and args.server and args.message):
        parser.error("needs more arguments")
        sys.exit()
    else:
        # save user arguments
        argumendariuser = {
            'sender': args.sender,
            'passwd': args.passwd,
            'to': args.to,
            'server': args.server,
            'port': args.port,
            'ssl': args.ssl,
            'subject': args.subject,
            'message': args.message,
            'attachment': args.attachment,
            'attachdir': args.attachdir, }
        return argumendariuser


def print_users_arguments(usersarguments):
    # pretty print users arguments
    pprint.pprint(usersarguments)


def parse_config_file(config_file):
    config = ConfigParser.ConfigParser()
    config.read(config_file)
    section_list = config.sections()
    print "Reading Config = ", section_list
    # get option value
    config_values = {}
    # if section list is not empty
    if section_list:
        for section in section_list:
            dict1 = {}
            options = config.options(section)
            for option in options:
                try:
                    dict1[option] = config.get(section, option)
                    if dict1[option] == -1:
                        DebugPrint("skip: %s" % option)
                except:
                    print("exception on %s!" % option)
                    dict1[option] = None
            config_values[section] = dict1         
        return config_values
    else:
        print "Error. Config file is not valid"
        sys.exit(1)


def send_email(
        senderaddress,
        senderemailpassword,
        recipientaddress,
        smtpsetting,
        body,
        attachment,
        smtpport=25,
        secureemail=False,
        subject="",
        attach_all_in_directory=False):
    
    #print status
    print "================"
    print "Sending E-mail on progress. Make sure your settings are correct"
    # check operating system that used to run this script
    # decide which directory separator should be used
    if platform.system == "Windows":
        dirseparator = "\\"
    else:
        dirseparator = "/"

    # check whether the recipient(s) address inputted is a string or a list
    # if recipient address is a string then create a list from it
    if isinstance(recipientaddress, basestring):
        recipientaddress = recipientaddress.replace(" ", "").split(",")
    
    print "Create a list of recipients: ", recipientaddress

    # create a new MIME Multipart object
    # create from, to, and subject of the email
    message = MIMEMultipart()
    message['From'] = senderaddress
    message['To'] = ", ".join(recipientaddress)
    message['Subject'] = subject
    message.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    message.attach(MIMEText(body, 'plain'))

    # handle attachment(s)
    # check if there will be any attachment in the email. 
    # if attach_all_in_directory is True then all items in the directory will be attached.
    if attach_all_in_directory:
        print "Attach all items in this directory = ", str(attachment)
        if not attachment:
            directory = "." + dirseparator
            print "(3) " + str(directory)
        elif os.path.isfile(attachment):
            directory = attachment.split(dirseparator)[:-1]
            if len(directory) == 0:
                directory = "." + dirseparator
            print "(1) " + str(directory)
        elif attachment:
            directory = attachment
            print "(2) " + str(directory)
        # iteration source: items in directory
        iteration = os.listdir(directory)
    # if attachment is not NULL but attach all in directory flag not set
    elif attachment:
        print "Create a list of attachments = ", str(attachment)
        attachment = attachment.replace(", ", ",").split(",")
        iteration = attachment
    # if attachment is NULL
    else:
        print "No attachment set"
        iteration = False

    # if attachment is set
    if iteration:
        for filename in iteration:
            if attach_all_in_directory:
                path = os.path.join(directory, filename)
                if not os.path.isfile(path):
                    str(filename) + " is not a file"
                    continue
            else:
                path = filename
                filename = path.split(dirseparator)[-1]
                if not os.path.isfile(path):
                    print str(filename) + " is not a valid file."
                    continue
            # Guess the content type based on the file's extension.  Encoding
            # will be ignored, although we should check for simple things like
            # gzip'd or compressed files.
            ctype, encoding = mimetypes.guess_type(path)
            if ctype is None or encoding is not None:
                # No guess could be made, or the file is encoded (compressed), so
                # use a generic bag-of-bits type.
                ctype = 'application/octet-stream'
            maintype, subtype = ctype.split('/', 1)
            if maintype == 'text':
                fp = open(path)
                # Note: we should handle calculating the charset
                msg = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == 'image':
                fp = open(path, 'rb')
                msg = MIMEImage(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == 'audio':
                fp = open(path, 'rb')
                msg = MIMEAudio(fp.read(), _subtype=subtype)
                fp.close()
            else:
                fp = open(path, 'rb')
                msg = MIMEBase(maintype, subtype)
                msg.set_payload(fp.read())
                fp.close()
                # Encode the payload using Base64
                encoders.encode_base64(msg)
            # Set the filename parameter
            msg.add_header('Content-Disposition', 'attachment', filename=filename)
            message.attach(msg)
            # print attached file
            print str(filename) + " has been attached"

    text = message.as_string()
    mailserver = smtplib.SMTP(smtpsetting, smtpport)

    if secureemail is True:
        print "SSL will be used"
        mailserver.starttls()

    print "================"
    print "Connecting to server"
    mailserver.login(senderaddress, senderemailpassword)
    print "Sending email"
    mailserver.sendmail(senderaddress, recipientaddress, text)
    mailserver.quit()
    return


def startprogram():
    # start time
    starttime = time.time()
    # print status
    print "================"
    print "Parsing Argumets/Settings"
    # get arguments from user
    usersarguments = argumentsparsing()
    # print user's arguments
    print_users_arguments(usersarguments)
    # get browser-local-storage
    send_email(
        usersarguments['sender'],
        usersarguments['passwd'],
        usersarguments['to'],
        usersarguments['server'],
        usersarguments['message'],
        usersarguments['attachment'],
        usersarguments['port'],
        usersarguments['ssl'],
        usersarguments['subject'],
        usersarguments['attachdir'])
    # print total execution time
    print("Completed in --- %s seconds ---" % (time.time() - starttime))
    print "================"
    # exiting
    return


if __name__ == "__main__":
    # execute main function
    startprogram()
    sys.exit()
